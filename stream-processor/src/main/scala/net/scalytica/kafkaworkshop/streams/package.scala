package net.scalytica.kafkaworkshop

package object streams {

  implicit def mapToProperties(m: Map[String, String]): java.util.Properties = {
    val props = new java.util.Properties

    m.foreach { case (key, value) => props.put(key, value) }

    props
  }

}
