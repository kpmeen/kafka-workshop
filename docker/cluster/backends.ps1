param (
    [Parameter(Mandatory=$true)][string]$cmd
)

Write-Output "   __    __           _        _                           "
Write-Output "  / / /\ \ \___  _ __| | _____| |__   ___  _ __            "
Write-Output "  \ \/  \/ / _ \| '__| |/ / __| '_ \ / _ \| '_ \           "
Write-Output "   \  /\  / (_) | |  |   <\__ \ | | | (_) | |_) |          "
Write-Output "    \/  \/ \___/|_|  |_|\_\___/_| |_|\___/| .__/           "
Write-Output "                                          |_|              "
Write-Output "   __           _                                      _   "
Write-Output "  /__\ ____   _(_)_ __ ___  _ __  _ __ ___   ___ _ __ | |_ "
Write-Output " /_\| '_ \ \ / / | '__/ _ \| '_ \| '_ \` _ \ / _ \ '_ \| __|"
Write-Output "//__| | | \ V /| | | | (_) | | | | | | | | |  __/ | | | |_ "
Write-Output "\__/|_| |_|\_/ |_|_|  \___/|_| |_|_| |_| |_|\___|_| |_|\__|"
Write-Output "                                                           "

Write-Output "not really finished.... do not use fix ip stuff"
$env:HOST_IP = "127.0.0.1"

function Start-Docker([int]$v)
{
    Write-Output "Starting docker-compose build..."
    docker-compose up -d --build --force-recreate

    Write-Output "Initializing topics..."

    Start-Sleep -seconds 10

    docker exec -it cluster_kafka1_1 kafka-topics `
    --zookeeper zookeeper:22181 `
    --create `
    --topic workshop-input-topic `
    --partitions 1 `
    --replication-factor 1

    docker exec -it cluster_kafka1_1 kafka-topics `
    --zookeeper zookeeper:22181 `
    --create `
    --topic workshop-aggregated-topic `
    --partitions 1 `
    --replication-factor 1

    Write-Output "Kafka cluster is ready!"
}

function Stop-Docker([int]$v)
{
    Write-Output "Stopping services..."
    docker-compose down --remove-orphans --rmi local
}

if($cmd -eq "start")
{
    Start-Docker(0)
}
elseif($cmd -eq "stop")
{
    Stop-Docker(0)
}
elseif($cmd -eq "restart")
{
    Stop-Docker(0)
    Start-Docker(0)
}
else
{
    Write-Output "Usage: backends.ps1 -cmd <start | stop | restart>"
}